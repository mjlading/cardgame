package edu.ntnu.idatt2001.lading.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {

    @Test
    @DisplayName("A new deck has 52 cards")
    public void ANewDeckHas52Cards() {
        DeckOfCards deck = new DeckOfCards();

        assertEquals(52, deck.getPlayingCards().size());
    }

    @Nested
    @DisplayName("When argument is invalid")
    public class InvalidArgument {

        @Test
        @DisplayName("Dealing more than 52 cards throws exception")
        public void dealingMoreThan52Cards() {
            DeckOfCards deck = new DeckOfCards();

            assertThrows(IllegalArgumentException.class,
                    () -> deck.dealHand(53),
                    "Expected dealing more than 52 cards to throw IllegalArgumentException, but it didn't"
            );
        }
    }
}
