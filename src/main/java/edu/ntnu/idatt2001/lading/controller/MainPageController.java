package edu.ntnu.idatt2001.lading.controller;

import edu.ntnu.idatt2001.lading.model.HandOfCards;
import edu.ntnu.idatt2001.lading.model.PlayingCard;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;

/**
 * Controller class for the main page.
 */
public class MainPageController {

    private HandOfCards currentHand;

    @FXML
    private Text handsDisplay;

    @FXML
    private Text flushText;

    @FXML
    private Text sumOfFacesText;

    @FXML
    private Text cardsOfHearts;

    @FXML
    private Text queenOfSpades;

    @FXML
    private Button dealHandButton;

    @FXML
    private Button checkHandButton;

    @FXML
    public void initialize() {

        dealHandButton.setOnAction((event) -> dealHandButtonClicked());
        checkHandButton.setOnAction((event) -> checkHandButtonClicked());
    }

    private void dealHandButtonClicked() {
        currentHand = new HandOfCards();

        handsDisplay.setText("Current hand:\n" + currentHand.toString());
    }

    private void checkHandButtonClicked() {

        // Check for flush
        flushText.setText((currentHand.hasFlush() ? "Yes" : "No"));

        // Check sum of the faces
        sumOfFacesText.setText(Integer.toString(currentHand.sumOfFaces()));

        // Check number of cards of hearts
        StringBuilder sb = new StringBuilder();
        for (PlayingCard card : currentHand.getCardsOfHearts()) {
            sb.append(card.getAsString()).append(" ");
        }
        cardsOfHearts.setText((currentHand.getCardsOfHearts().isEmpty()) ? "None" : sb.toString());

        // Check if deck contains queen of spades card
        queenOfSpades.setText((currentHand.hasQueenOfSpades()) ? "Yes" : "No");
    }
}
