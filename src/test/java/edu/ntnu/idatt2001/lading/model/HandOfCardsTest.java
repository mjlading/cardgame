package edu.ntnu.idatt2001.lading.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

    @Test
    @DisplayName("A new hand has the correct number of cards")
    public void newHandHasCorrectNumberOfCards() {
        HandOfCards hand = new HandOfCards();
        int expected = hand.getINITIAL_HAND_SIZE();

        assertEquals(expected, hand.getPlayingCards().size());
    }
}