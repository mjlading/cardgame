package edu.ntnu.idatt2001.lading;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {

    public static void main(String[] args) {

        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource('\u002f' + "view" + '\u002f' +
                "main_page_view.fxml"));

        Parent root = loader.load();
        Scene initialScene = new Scene(root);

        primaryStage.setTitle("Card game");
        primaryStage.setScene(initialScene);
        primaryStage.show();
    }
}
