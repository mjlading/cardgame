package edu.ntnu.idatt2001.lading.model;

import java.util.*;

/**
 * Class that represents a full deck of playing cards (52 cards).
 */
public class DeckOfCards {

    private final List<PlayingCard> playingCards;
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private static final int NUMBER_OF_FACES = 13;

    /**
     * Instantiate a new deck of all 52 playing cards.
     */
    public DeckOfCards() {
        playingCards = new ArrayList<>();

        for (int i = 1; i <= NUMBER_OF_FACES; i++) {
            for (char suit : suit) {
                playingCards.add(new PlayingCard(suit, i));
            }
        }
    }

    /**
     * Returns a given number of random cards from the deck.
     * The dealt cards are removed from the deck.
     *
     * @param n The number of cards to deal from the deck
     * @return A collection of randomly picked cards
     */
    public ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException {
        if (n > 52) {
            throw new IllegalArgumentException("Cannot deal more cards than the deck contains (52).");
        }

        ArrayList<PlayingCard> randomCards = new ArrayList<>();
        Random random = new Random();

        while (n > 0) {
            PlayingCard randomPlayingCard = playingCards.get(random.nextInt(playingCards.size()));
            randomCards.add(randomPlayingCard);
            n--;
        }

        return randomCards;
    }

    /**
     * Get all cards in the hand.
     *
     * @return A list of cards in the hand
     */
    public List<PlayingCard> getPlayingCards() {
        return playingCards;
    }
}
