package edu.ntnu.idatt2001.lading.model;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class that represents a player's hand of cards.
 */
public class HandOfCards {

    private final List<PlayingCard> playingCards;
    private final int INITIAL_HAND_SIZE = 13; // Number of cards to be dealt to the hand

    /**
     * Instantiate a random hand of cards from a 52-card deck.
     */
    public HandOfCards() {
        playingCards = new DeckOfCards().dealHand(INITIAL_HAND_SIZE);
    }

    /**
     * Calculate the sum of the card faces of the hand.
     *
     * @return A number for the sum of card faces
     */
    public int sumOfFaces() {
        return playingCards.stream()
                .mapToInt(PlayingCard::getFace)
                .sum();
    }

    /**
     * Check for 5-card flush (5 cards or more of the same suit).
     *
     * @return True or false, if the hand has a flush or not
     */
    public boolean hasFlush() {
        return playingCards.stream()
                .anyMatch(card -> playingCards.stream()
                        .filter(c -> c.getSuit() == card.getSuit())
                        .count() >= 5);
    }

    /**
     * Get all cards of hearts in the hand.
     *
     * @return A list of all cards of hearts
     */
    public List<PlayingCard> getCardsOfHearts() {
        return playingCards.stream()
                .filter(card -> card.getSuit() == 'H')
                .collect(Collectors.toList());
    }

    /**
     * Check if the hand contains the queen of spades card.
     *
     * @return True or false, if the hand contains the queen of spades or not
     */
    public boolean hasQueenOfSpades() {
        return playingCards.stream()
                .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    }

    /**
     * Returns all the playing cards in the hand.
     *
     * @return List of playing cards in the hand
     */
    public List<PlayingCard> getPlayingCards() {
        return playingCards;
    }

    /**
     * Get the initial hand size for the hand.
     *
     * @return A number for the initial hand size of the hand
     */
    public int getINITIAL_HAND_SIZE() {
        return INITIAL_HAND_SIZE;
    }

    /**
     * Custom toString() method that displays all the cards of the hand.
     * The cards are displayed on the format: "H4 H12 C3 D11 S1".
     *
     * @return A string representing the hand of cards
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (PlayingCard playingCard : playingCards) {
            sb.append(playingCard.getAsString()).append(" ");
        }
        return sb.toString();
    }
}
